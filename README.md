# Repository description

contain all maintenance script for TNRO system

1. [line notify server connection](check_connection/readme.md)
2. [script shutdown all servers](shutdown_server/readme.md)


# Floder description
1. check_connection: -
2. document: -
3. logger: keep log from TNRO system that can be looking forward
4. shutdown_server: -
5. supervisor: keep service that runnung on TNRO server(N-01) 
    - etc_supervisor: config file using on ubuntu /etc/supervisor
    - mongodb: docker compose for running mongodb service. it is the database for keep add sensors
    - receiver
        - K_band: temperature sensor for K band dwar
        - L_band: temperature sensor for L band dwar


# How to

how to add new supervisord service `http://192.168.90.1:9001`
1. add new xxx.conf in `/etc/supervisor/conf.d` 
2. update supervisord service by
    - sudo supervisorctl reread
    - sudo supervisorctl update

    Note! -> some machine need to specifix with pip path `sudo pip install --target=/usr/local/lib/python2.7/dist-packages <packagename>`