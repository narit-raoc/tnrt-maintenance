import pymongo
from pymongo import MongoClient
import datetime 
import pandas # sudo apt install python3-pandas, sudo pip3 install pandas
import os

today = datetime.date.today()
yesterday = today - datetime.timedelta(days = 1)
twoday_ago = today - datetime.timedelta(days = 2)

print(today)
print(yesterday)

# Server Parameters
client = pymongo.MongoClient('mongodb://root:rootpassword@localhost:27017/')
db = client.visalar_monitor # use database myDB
coll = db.Monitor


last_N = 10

# query last_N data from collection
data_row = coll.find().skip(coll.count() - last_N)
for x in data_row:
  print x

# data_row = coll.find_one()
# for data_row in coll.find(): 
#     print(data_row)   
