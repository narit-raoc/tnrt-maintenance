__author__ = "Teep Chairin"
__copyright__ = "Copyright 2020, The visailar exporter"
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "-"
__email__ = "teep@narit.or.th"
__status__ = "production"


"""
history
- 23.11.22 first create
- 24.11.22 deploy production on 90.1 with supervisord
"""

import sys
import requests
import serial
from time import time, sleep
import datetime
import threading

import pandas # sudo apt install python3-pandas, sudo pip3 install pandas
import os

import pymongo
from pymongo import MongoClient


url = 'https://notify-api.line.me/api/notify'
token_Backend_alarm = 'OqMzFbzExLRSbIYoLsd1zvS4SrFwsUtsmZV18ZdBUDW'
headers_Backend_alarm = {
            'content-type':
            'application/x-www-form-urlencoded',
            'Authorization':'Bearer '+token_Backend_alarm
           }

msg_error = ""

delta = datetime.timedelta(hours=1)
now = datetime.datetime.now()
next_hour = (now + delta).replace(microsecond=0, second=0, minute=2)
wait_seconds = (next_hour - now).seconds

msg = "start service export sensor to csv: " + str(now) + " and will start the loop in next 1 hour"
print(msg)

send_to_Backend_alarm_thatProgramStar = requests.post(url, headers=headers_Backend_alarm , data = {'message':msg})
sleep(wait_seconds)
     
secound = 60

def export_csv():
    global client
    global db
    global coll
    global data_row
    global flagdataisnull
    global flagcheck
    global msg_error
    global secound
    '''
    state info:
    -1 = error state
    0 = provision 
    1 = wait 24hr after start this service to read data from mongo and fillter and export to csv file
    2 = deprovision
    '''

    print("start loop")
    state = 0
    while True:
        if(state == -1):
            # error state
            msg = "[SYSTEM ERROR ExportVisalar]: " + msg_error
            r = requests.post(url, headers=headers_Backend_alarm , data = {'message':msg})
            state = 0
 
        if(state == 0): # 0 = provision state
            # step1 connect to mongo if found any error fix it  
            # sleep((secound - time() % secound))
            sleep((secound - time() % secound) * 60 * 24) # 60 is 1hr, 24 is day
            try:
                # step1
                client = pymongo.MongoClient('mongodb://root:rootpassword@localhost:27017/')
                db = client.visalar_monitor # use database myDB
                coll = db.Monitor
            except Exception as error:
                msg_error = "state 0 " + "msg: " + str(error)
                print(msg_error)
                state = -1
            
            state = 1       

        if(state == 1): 
            # step1 read data from mongo at 2 day ago
            # step2 validate data and export
            try:
                # step1
                today = datetime.date.today()
                yesterday = today - datetime.timedelta(days = 1)
                twoday_ago = today - datetime.timedelta(days = 2)

                # print("today: " + str(today))
                # print("yesterday: "+ str(yesterday))
                # print("2 day ago: "+ str(twoday_ago))

                data_row = coll.find( {'date': {"$gte":  datetime.datetime(yesterday.year, yesterday.month, yesterday.day, 0,0,0), "$lte": datetime.datetime(yesterday.year, yesterday.month, yesterday.day,23,59,59)}})
                
                # step2
                count = data_row.count() # length of mongo array
                # print(str(yesterday))

                # Convert the mongo docs to a DataFrame
                data_export = pandas.DataFrame(data_row)
                # Discard the Mongo ID for the documents
                data_export.pop("_id")

                output_dir = "/mnt/tnrt/dev/log/visalar/"
                output_filename = "visalar_log_" + str(yesterday) +".csv"
                output_path = output_dir + output_filename
                print("output_path: " + output_path)
                isExist = os.path.exists(output_path)

                if(isExist == True):
                    print("file name already exist on this path")
                else:
                    # export MongoDB documents to a CSV file, leaving out the row "labels" (row numbers)
                    data_export.to_csv(output_path, ",", index=False) # CSV delimited by commas
                    print("Export done")
        
            except Exception as error:
                msg_error = "state 1 " + "msg: " + str(error)
                print(msg_error)
                state = -1

            state = 2
        
        if(state == 2):
            # step1 disconnect mongodb
            try:
                client.close()
            except Exception as error:
                print("write_sensor_to_mongo " + error)
                msg_error = "state 2 can't write mongo" + "msg: " + str(error)
                state = -1

            state = 0

if __name__ == '__main__':
    export_csvThread = threading.Thread(target=export_csv)
    export_csvThread.daemon = True
    export_csvThread.start()

    while True:
        try:
            sleep(2)
        except KeyboardInterrupt:
            sleep(2)
            print("kill")
            # timerThread.join(0.1)       
            export_csvThread.join(0.1)
            # SendEvery12HourThread.join(0.1)
            # SendEvery10MinThread.join(0.1)
            sys.exit()




# add this script to supervisor
# add grehp