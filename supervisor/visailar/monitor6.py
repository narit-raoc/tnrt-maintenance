__author__ = "Teep Chairin"
__copyright__ = "Copyright 2020, The visailar mornitor"
__license__ = "GPL"
__version__ = "4.1"
__maintainer__ = "-"
__email__ = "teep@narit.or.th"
__status__ = "Test"


"""
history
- version 1
- version 2
- version 3
- version 4
    - version 4.1
        - send alarm to only "Backend_alarm" line group
- version 5
    - add mongodb database

- 31/10/22 : bug monitor after mongodb stop and can't write sensor
- 1/11/22 : add msg for send error to line too
- 13/01/23 : change ttyUSB0 to ttyUSB1

"""

import sys
import requests
import serial
from time import time, sleep
import datetime
import threading

import pymongo
from pymongo import MongoClient


url = 'https://notify-api.line.me/api/notify'
token = 'PT6ljx9jkxOYhZzCtYjCIxUnSmN3OqQc14RtfmwEviC'
token_Backend_alarm = 'OqMzFbzExLRSbIYoLsd1zvS4SrFwsUtsmZV18ZdBUDW'

headers = {
            'content-type':
            'application/x-www-form-urlencoded',
            'Authorization':'Bearer '+token
           }

headers_Backend_alarm = {
            'content-type':
            'application/x-www-form-urlencoded',
            'Authorization':'Bearer '+token_Backend_alarm
           }

# global variable
ser = serial.Serial(port = "/dev/ttyUSB0", baudrate=9600,
             bytesize=8, timeout=5, stopbits=serial.STOPBITS_ONE)
ser.flushInput()
print("Check serail is connected: " + str(ser))

flagcheck = True
flagsend = True
flagdataisnull =  True
count = 0
secound = 60
temp = 0
humid = 0

msg_error = ""

delta = datetime.timedelta(hours=1)
now = datetime.datetime.now()
next_hour = (now + delta).replace(microsecond=0, second=0, minute=2)
wait_seconds = (next_hour - now).seconds

# Mongo Server Parameters
client = pymongo.MongoClient('mongodb://root:rootpassword@localhost:27017/')
db = client.visalar_monitor # use database myDB
coll = db.Monitor

temperatureStats = {
        "sensorName": "Visalar",
        "Temp": None,
        "Humid": None,
        "date": datetime.datetime.now()
    }

msg = "start service Visalar monitoring: " + str(now) + " and will start the loop in next 1 h"
print(msg)

send_to_Backend_alarm_thatProgramStar = requests.post(url, headers=headers , data = {'message':msg})
send_to_Backend_alarm_thatProgramStar = requests.post(url, headers=headers_Backend_alarm , data = {'message':msg})

sleep(wait_seconds)

now = datetime.datetime.now()

print("start loop service in next hour " + str(now))

def send_every_10min():
    global flagsend
    global temp 
    global humid
    state = 0
    while True:
        if(state == 0):
            sleep(60*10)       
            state = 1
        if(state == 1):
            # do case 1 in loop when flagdataisnull is True
            # if send requests.post done do next state again
            sleep(1)
            if(flagdataisnull == False):            
                try:
                    msg = "[SYSTEM LOG]:temp " + str(temp)+" humid:"+ str(humid)
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    print(msg)
                    print(r.text)
                    print("DONE, send noti every 10 min")
                    state = 0      
                except Exception as e:
                    print("ERROR, send every 10 min :" + str(e))    

def send_every_1hour():
    global flagsend
    global temp 
    global humid
    state = 0
    while True:
        if(state == 0):
            sleep(60*60*1)  
            state = 1
        if(state == 1):
            # do case 1 in loop when flagdataisnull is True
            # if send requests.post done do next state again
            if(flagdataisnull == False):            
                try:
                    msg = "[SYSTEM LOG]:temp " + str(temp)+" humid:"+ str(humid)
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    print(msg)
                    print(r.text)
                    print("DONE, send noti every 1 hr")
                    flagsend = False
                    state = 0      
                except Exception as e:
                    print("ERROR, send every 1 hr :" + str(e))    
        
def send_every_6hour():
    global flagsend
    global temp 
    global humid
    state = 0
    while True:
        if(state == 0):
            sleep(60*60*6)
            state = 1
        if(state == 1):
            # do case 1 in loop when flagdataisnull is True
            # if send requests.post done do next state again
            if(flagdataisnull == False):            
                try:
                    msg = "[SYSTEM LOG]:temp " + str(temp)+" humid:"+ str(humid)
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    print(msg)
                    print(r.text)
                    print("DONE, send noti every 6 hr")
                    flagsend = False
                    state = 0      
                except Exception as e:
                    print("ERROR, send every 6 hr :" + str(e))    
    
def send_every_12hour():
    global flagsend
    global temp 
    global humid
    state = 0
    while True:
        if(state == 0):
            sleep(60*60*12)    
            state = 1
        if(state == 1):
            # do case 1 in loop when flagdataisnull is True
            # if send requests.post done do next state again
            if(flagdataisnull == False):            
                try:
                    msg = "[SYSTEM LOG]:temp " + str(temp)+" humid:"+ str(humid)
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    print(msg)
                    print(r.text)
                    print("DONE, send noti every 12 hr")
                    flagsend = False
                    state = 0      
                except Exception as e:
                    print("ERROR, send every 12 hr :" + str(e))     
        
def timmer():
    global count
    global flagsend
    while True:
        sleep(secound - time() % secound)
        count += 1
        print("count : " + str(count))
        if(count >= 1):
            flagsend = True
            count = 0

def read_and_check():
    global count
    global temp
    global humid
    global flagdataisnull
    global flagcheck
    global msg_error
    '''
    state info:
    -1 = error state
    0 = do every 1 min to read data from receiver
    1 = check condition
    2 = write databese to mongo
    '''
    state = 0
    while True:
        if(state == -1):
            # error state
            msg = "[SYSTEM ERROR]: " + msg_error
            r = requests.post(url, headers=headers , data = {'message':msg})
            ser_bytes = ""
            humid = 0.0
            temp = 0.0
            flagdataisnull = True
            state = 0
 
        if(state == 0):
            # do every 1 minuite
            sleep(secound - time() % secound)
            # clear data 
            ser_bytes = ""
            # read visailar
            try:
                ser_bytes = ser.readline()
                # print(str(ser_bytes))
                # print(str(ser_bytes[0]))
                # sub string 
                if(ser_bytes != "" and ser_bytes[0] == "R"):
                    humid = ser_bytes[4:8]
                    temp = ser_bytes[16:20]
                    humid = float(humid)
                    temp = float(temp)
                    print("temp : " + str(temp) + " humid : " + str(humid))
                    flagdataisnull = False
                    state = 1
                else:
                    flagdataisnull = True
                    print("ser_bytes = null")
                    state = 0
            except Exception as e:
                print("serail error:" + str(e))
                   

        if(state == 1): 
            # check condition if system normal
            try:      
                if(flagcheck == False and (temp < 28 and humid < 75)):
                    msg = "[SYSTEM INFO]:The system's back to normal."
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    print(msg)
                    print(r.text)
                    flagcheck = True
            except Exception as e:
                print("check normal error:" + str(e))
                msg_error =  "state: " + str(state) + " msg: " + str(e)
                state = -1

            # if system error let send alram every minute
            try:
                if(flagcheck == False):
                    msg = "[SYSTEM ERROR visalar] temp:"+ str(temp)+" humid:"+ str(humid)
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    send_to_Backend_alarm = requests.post(url, headers=headers_Backend_alarm , data = {'message':msg})
                    print(msg)
                    print(r.text)        
            except Exception as e:
                print("if system error notify every minute error:" + str(e))   
                msg_error =  "state: " + str(state) + " msg: " + str(e)
                state = -1

            # check condition of temp and humid are error?
            try:
                if(flagcheck == True and (temp >= 27 or humid >= 80)):
                    msg = "[SYSTEM ERROR visalar]:The system has a malfunction." + "temp:" + str(temp)+" humid:"+ str(humid)
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    send_to_Backend_alarm = requests.post(url, headers=headers_Backend_alarm , data = {'message':msg})
                    print(msg)
                    print(r.text)
                    flagcheck = False            
            except Exception as e:
                print("check abnormal error:" + str(e)) 
                msg_error =  "state: " + str(state) + " msg: " + str(e)
                state = -1

            state = 2
        
        if(state == 2):
            temperatureStats = {
                "sensorName": "Visalar",
                "Temp": int(temp),
                "Humid": int(humid),
                "date": datetime.datetime.now()
                }

            try:
                coll.insert_one(temperatureStats)
                # print("write to mongo done")                    
            except Exception as error:
                print("write_sensor_to_mongo " + error)
                msg_error = "can't write mongo" + "msg: " + str(error)
                state = -1

            state = 0
     
if __name__ == '__main__':
    # timerThread = threading.Thread(target=timmer)
    # timerThread.daemon = True
    # timerThread.start()

    read_and_checkThread = threading.Thread(target=read_and_check)
    read_and_checkThread.daemon = True
    read_and_checkThread.start()

    # SendEvery10MinThread = threading.Thread(target=send_every_10min)
    # SendEvery10MinThread.daemon = True
    # SendEvery10MinThread.start()

    # SendEvery1HourThread = threading.Thread(target=send_every_1hour)
    # SendEvery1HourThread.daemon = True
    # SendEvery1HourThread.start()

    # SendEvery6HourThread = threading.Thread(target=send_every_6hour)
    # SendEvery6HourThread.daemon = True
    # SendEvery6HourThread.start()

    SendEvery12HourThread = threading.Thread(target=send_every_12hour)
    SendEvery12HourThread.daemon = True
    SendEvery12HourThread.start()

    while True:
        try:
            sleep(2)
        except KeyboardInterrupt:
            print("kill")
            # timerThread.join(0.1)       
            read_and_checkThread.join(0.1)
            SendEvery12HourThread.join(0.1)
            # SendEvery10MinThread.join(0.1)
            ser.flushInput()    # clear serial port
            ser.close()
            sys.exit()




# add this script to supervisor
# add grehp
# sudo service supervisord reload
# sudo supervisorctl reread
# sudo supervisorctl update
