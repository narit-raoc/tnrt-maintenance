import pymongo
from pymongo import MongoClient
import datetime 
import pandas # sudo apt install python3-pandas, sudo pip3 install pandas
import os

today = datetime.date.today()
yesterday = today - datetime.timedelta(days = 1)
twoday_ago = today - datetime.timedelta(days = 2)

print(today)
print(yesterday)

# Server Parameters
client = pymongo.MongoClient('mongodb://root:rootpassword@localhost:27017/')
db = client.visalar_monitor # use database myDB
coll = db.Monitor



# mongo_docs = coll.find( {'date': {"$gte":  datetime.datetime(twoday_ago.year, twoday_ago.month, twoday_ago.day), "$lte": datetime.datetime(yesterday.year, yesterday.month, yesterday.day)}})


try:
    mongo_docs = coll.find( {'date': {"$gte":  datetime.datetime(yesterday.year, yesterday.month, yesterday.day, 0,0,0), "$lte": datetime.datetime(yesterday.year, yesterday.month, yesterday.day,23,59,59)}})              
except Exception as e:
    print(str(e))

print mongo_docs

if(mongo_docs.count() > 0):
    print(mongo_docs[0]['date'])

    print(type(mongo_docs))

    x = mongo_docs.count()


    print(x)

    print("today: " + str(today))
    print("yesterday: "+ str(yesterday))
    print("2 day ago: "+ str(twoday_ago))

    print(mongo_docs[0]['date'])
    print(mongo_docs[0]['date'].date())
    print(mongo_docs[x-1]['date'])
    print(mongo_docs[x-1]['date'].date())
    # Convert the mongo docs to a DataFrame
    docs = pandas.DataFrame(mongo_docs)
    # Discard the Mongo ID for the documents
    docs.pop("_id")

    # print(docs)

    # output_dir = "/mnt/tnrt/dev/log/"
    output_dir = "./"
    output_filename = "visalar_log_" + str(yesterday) +".csv"

    output_path = output_dir + output_filename

    print(output_path)
    isExist = os.path.exists(output_path)
    print(isExist)

    if(isExist == True):
        print("file name already exist on this path")
    else:
        # export MongoDB documents to a CSV file, leaving out the row "labels" (row numbers)
        docs.to_csv(output_path, ",", index=False) # CSV delimited by commas
        print("Export done")
else:
    print("no logger file yesterday")
   

