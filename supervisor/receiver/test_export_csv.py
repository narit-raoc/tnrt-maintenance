import pymongo
from pymongo import MongoClient
import datetime 
import pandas # sudo apt install python3-pandas, sudo pip3 install pandas
import os

'''
Note: 
this code for test read data from real receiver's temperature from  L and K sensor, after that try to export to this path "./"
- check date export only yesterday because already have data from 00.00 to 23.59 
- connect to mongodb
- read data from the perious
- export K if data_array not null
- export L if data_array not null
'''

today = datetime.date.today()
yesterday = today - datetime.timedelta(days = 1)
twoday_ago = today - datetime.timedelta(days = 2)

print(today)
print(yesterday)

# Server Parameters
client = pymongo.MongoClient('mongodb://root:rootpassword@localhost:27017/')
db = client.LK_monitor # use database myDB
coll = db.Monitor

# mongo_docs = coll.find( {'date': {"$gte":  datetime.datetime(twoday_ago.year, twoday_ago.month, twoday_ago.day), "$lte": datetime.datetime(yesterday.year, yesterday.month, yesterday.day)}})
mongo_docs_K = coll.find( {'sensorName':"Kband" ,'date': {"$gte":  datetime.datetime(yesterday.year, yesterday.month, yesterday.day, 0,0,0), "$lte": datetime.datetime(yesterday.year, yesterday.month, yesterday.day,23,59,59)}})
mongo_docs_L = coll.find( {'sensorName':"Lband" ,'date': {"$gte":  datetime.datetime(yesterday.year, yesterday.month, yesterday.day, 0,0,0), "$lte": datetime.datetime(yesterday.year, yesterday.month, yesterday.day,23,59,59)}})

data_array_K = mongo_docs_K.count()
data_array_L = mongo_docs_L.count()

print("data_array_K: " + str(data_array_K))
print("data_array_L: " + str(data_array_L))

# Export K band
if(data_array_K > 0 ):
    print(type(mongo_docs_K))

    # print(mongo_docs_K[0]['sensorName'])
    # print(mongo_docs_K[0]['date'])

    print("today: " + str(today))
    print("yesterday: "+ str(yesterday))
    print("2 day ago: "+ str(twoday_ago))

    print(mongo_docs_K[0]['date'])
    print(mongo_docs_K[0]['date'].date())
    print(mongo_docs_K[data_array_K-1]['date'])
    print(mongo_docs_K[data_array_K-1]['date'].date())
    # Convert the mongo docs to a DataFrame
    docs = pandas.DataFrame(mongo_docs_K)
    # Discard the Mongo ID for the documents
    docs.pop("_id")

    print(docs)

    output_dir = "./"
    output_filename = "Ktemp_log_" + str(yesterday) +".csv"

    output_path = output_dir + output_filename

    print(output_path)
    isExist = os.path.exists(output_path)

    if(isExist == True):
        print("file name already exist on this path")
    else:
        # export MongoDB documents to a CSV file, leaving out the row "labels" (row numbers)
        docs.to_csv(output_path, ",", index=False) # CSV delimited by commas
        print("Export done")

# Export L band
if(data_array_L > 0 ):
    print(type(mongo_docs_L))

    # print(mongo_docs_L[0]['sensorName'])
    # print(mongo_docs_L[0]['date'])

    print("today: " + str(today))
    print("yesterday: "+ str(yesterday))
    print("2 day ago: "+ str(twoday_ago))

    print(mongo_docs_L[0]['date'])
    print(mongo_docs_L[0]['date'].date())
    print(mongo_docs_L[data_array_L-1]['date'])
    print(mongo_docs_L[data_array_L-1]['date'].date())
    # Convert the mongo docs to a DataFrame
    docs = pandas.DataFrame(mongo_docs_L)
    # Discard the Mongo ID for the documents
    docs.pop("_id")

    print(docs)

    output_dir = "./"
    output_filename = "Ltemp_log_" + str(yesterday) +".csv"

    output_path = output_dir + output_filename

    print(output_path)
    isExist = os.path.exists(output_path)

    if(isExist == True):
        print("file name already exist on this path")
    else:
        # export MongoDB documents to a CSV file, leaving out the row "labels" (row numbers)
        docs.to_csv(output_path, ",", index=False) # CSV delimited by commas
        print("Export done")
