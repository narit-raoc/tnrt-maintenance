__author__ = "Teep Chairin"
__copyright__ = ""
__license__ = ""
__version__ = "2"
__maintainer__ = "-"
__email__ = "teep@narit.or.th"
__status__ = "prodution"

"""
history
- version 1.0 create valiable
    t15_alram_point = 23
    t70_alram_point = 50
    t15_normal_point = 18
    t70_normal_point = 45

- version 1.1 create another line api to send only alarm (superate alarm and log line application) 

- version 2.0 
    - add databese mongodb
    - add more state = 2 for write mongo 

- 31/10/22 : bug monitor after mongodb stop and can't write sensor to  
- 1/11/22 : add msg for send error to line too
"""

from katcp import BlockingClient, MessageParser
import time
import json

import sys
import requests
from time import time, sleep
import datetime
import threading

import pymongo
from pymongo import MongoClient

class KatcpClient(BlockingClient):
    def __init__(self, host, port):
        self.msg_parser = MessageParser()
        super(KatcpClient, self).__init__(host, port)
        self.start()

    def __del__(self):
        self.stop()
        self.join()

    def request(self, command):
        msg = self.msg_parser.parse(command.encode())
        return self.blocking_request(msg)

    def get_sensor_value(self, sensor):
        (reply, inform) = self.request("?sensor-value {}".format(sensor))
        self.check_reply_message(reply, inform)

        if len(inform) > 1 and len(inform[0].arguments) == 1:  # getting error
            return inform[0].arguments[0].decode()

        return inform[0].arguments[4].decode("utf-8")

    def check_reply_message(self, reply, inform):
        if reply.arguments[0].decode() == "invalid":
            raise RuntimeError("Error: {}".format(reply.arguments[1].decode()))

        if reply.arguments[0].decode() == "fail":
            raise RuntimeError("Error: {}".format(inform[0].arguments[0].decode()))

    def debug_message(self, reply, inform):
        print("===============reply===============")
        print("reply - {}".format(reply))
        print("mtype = {}".format(reply.mtype))
        print("name = {}".format(reply.name))
        print("arguments = {}".format(reply.arguments))
        print("mid = {}".format(reply.mid))
        print("---------inform-----------")
        for m in inform:
            print("mtype = {}".format(m.mtype))
            print("name = {}".format(m.name))
            print("arguments = {}".format(m.arguments))
            print("mid = {}".format(m.mid))
            print("------")

    def set_digitizer(self, val):
        if val not in [True, False]:
            raise ValueError("Value error: value should be boolean")
        if not self.is_connected():
            raise ConnectionError("Connection error: Device is not connected")

        convertedVal = None
        if val:
            convertedVal = "true"
        else:
            convertedVal = "false"

        (reply, inform) = self.request("?rxs-power-digitizer {}".format(convertedVal))
        self.check_reply_message(reply, inform)

    def set_down_converter(self, val):
        if val < 14000 or val > 212000:
            raise ValueError("Value error: value should be between 14000-212000")
        if not self.is_connected():
            raise ConnectionError("Connection error: Device is not connected")

        (reply, inform) = self.request("?rxs-dwnc-frequency {}".format(val))
        self.check_reply_message(reply, inform)

    def set_attenuation(self, chain, val):
        if val < 0 or val > 31.5 or chain not in ["1", "2"]:
            raise ValueError("Value error: chain should be 1 or 2 and value should be between 0-31.5")
        if not self.is_connected():
            raise ConnectionError("Connection error: Device is not connected")

        (reply, inform) = self.request("?rxs-signalprocessors-sp0{}-attenuation {}".format(chain, val))
        self.check_reply_message(reply, inform)

    def set_operation_mode(self, mode):
        if mode not in [
            "go-soft-off",
            "go-cold-operational",
            "go-debug",
            "do-regeneration-to-warm",
            "do-regeneration",
            "error-noncritical-stay-cold",
            "error-noncritical-soft-off",
            "error-critical",
        ]:
            raise ValueError("Value error: mode incorrect")
        if not self.is_connected():
            raise ConnectionError("Connection error: Device is not connected")

        (reply, inform) = self.request("?rxs-rsm-opmode {}".format(mode))
        self.check_reply_message(reply, inform)

    def set_mmic(self, chain, val):
        if val not in [True, False] or chain not in ["1", "2"]:
            raise ValueError("Value error: chain should be 1 or 2 and value should be boolean")
        if not self.is_connected():
            raise ConnectionError("Connection error: Device is not connected")

        convertedVal = None
        if val:
            convertedVal = "true"
        else:
            convertedVal = "false"
        (reply, inform) = self.request("?rxs-mmic-enable-mmic{} {}".format(chain, convertedVal))
        self.check_reply_message(reply, inform)

    def set_cryo_motor(self, val):
        if val < 45 or val > 90:
            raise ValueError("Value error: value should be between 45-90")
        if not self.is_connected():
            raise ConnectionError("Connection error: Device is not connected")
        (reply, inform) = self.request("?rxs-cryocooler-motorstartspeed {}".format(val))
        self.check_reply_message(reply, inform)

    def shutdown(self):
        self.set_digitizer(False)
        self.set_operation_mode("go-soft-off")

    def get_kband_data(self):
        if not self.is_connected():
            raise ConnectionError("Connection error: Device is not connected")

        tfr = {
            "level1pps" : self.get_sensor_value("rxs.tfr.level1pps"),
            "level100mhz" :self.get_sensor_value("rxs.tfr.level100mhz")
        }

        box_weather = {
            "t_cold": self.get_sensor_value("rxs.boxweather.tempcold"),
            "t_warm": self.get_sensor_value("rxs.boxweather.tempwarm"),
        }

        atc = {
            "t_object": self.get_sensor_value("rxs.atc.objecttemp"),
            "t_sink": self.get_sensor_value("rxs.atc.sinktemp"),
        }

        chain_a = {
            "status": self.get_sensor_value("rxs.mmic.enable.mmic1"),
            "attenuation": self.get_sensor_value("rxs.signalprocessors.sp01.attenuation"),
            "total_power": self.get_sensor_value("rxs.signalprocessors.sp01.totalpower"),
        }

        chain_b = {
            "status": self.get_sensor_value("rxs.mmic.enable.mmic2"),
            "attenuation": self.get_sensor_value("rxs.signalprocessors.sp02.attenuation"),
            "total_power": self.get_sensor_value("rxs.signalprocessors.sp02.totalpower"),
        }

        dewar = {
            "heater20k": self.get_sensor_value("rxs.dewar.heater20k"),
            "heater70k": self.get_sensor_value("rxs.dewar.heater70k"),
            "t15": self.get_sensor_value("rxs.tempvac.temp15k"),
            "t70": self.get_sensor_value("rxs.tempvac.temp70k"),
        }

        down_converter = {
            "freq": self.get_sensor_value("rxs.dwnc.frequency")
        }

        KbandData = {
            "tfr": tfr,
            "device_sts": self.get_sensor_value("rxs.device-status"),
            "valve_sts": self.get_sensor_value("rxs.vac-valve-open"),
            "box_weather": box_weather,
            "atc": atc,
            "chain_a": chain_a,
            "chain_b": chain_b,
            "dewar": dewar,
            "down_converter": down_converter,
        }
        return KbandData

    def get_lband_data(self):
        if not self.is_connected():
            raise ConnectionError("Connection error: Device is not connected")

        tfr = {
            "level1pps": self.get_sensor_value("rxs.tfr.level1pps"),
            "level100mhz":self.get_sensor_value("rxs.tfr.level100mhz")
        }

        box_weather = {
            "t_cold": self.get_sensor_value("rxs.boxweather.tempcold"),
            "t_warm": self.get_sensor_value("rxs.boxweather.tempwarm")
        }

        atc = {
            "t_object": self.get_sensor_value("rxs.atc.objecttemp"),
            "t_sink": self.get_sensor_value("rxs.atc.sinktemp")
        }

        chain_a = {
            "status": self.get_sensor_value("rxs.mmic.enable.mmic1"),
            "attenuation": self.get_sensor_value("rxs.signalprocessors.sp01.attenuation"),
            "total_power": self.get_sensor_value("rxs.signalprocessors.sp01.totalpower")
        }

        chain_b = {
            "status": self.get_sensor_value("rxs.mmic.enable.mmic2"),
            "attenuation": self.get_sensor_value("rxs.signalprocessors.sp02.attenuation"),
            "total_power": self.get_sensor_value("rxs.signalprocessors.sp02.totalpower")
        }

        dewar = {
            "heater20k": self.get_sensor_value("rxs.dewar.heater20k"),
            "heater70k": self.get_sensor_value("rxs.dewar.heater70k"),
            "t15": self.get_sensor_value("rxs.tempvac.temp15k"),
            "t70": self.get_sensor_value("rxs.tempvac.temp70k")
        }

        LbandData = {
            "tfr" : tfr,
            "device_sts": self.get_sensor_value("rxs.device-status"),
            "valve_sts": self.get_sensor_value("rxs.vac-valve-open"),
            "box_weather": box_weather,
            "atc": atc,
            "chain_a": chain_a,
            "chain_b": chain_b,
            "dewar": dewar
        }
        return LbandData

# global variable
url = 'https://notify-api.line.me/api/notify'
token = 'Gia8eLHWOuLpT8aWpKnIFeCkCM7xL37LaEYusT21569'
token_LKband_alarm = '0gKdMtuaRyXCBPGxlQpvypBo6fLdNlH1KzzGnhtxUMG'

headers = {
            'content-type':
            'application/x-www-form-urlencoded',
            'Authorization':'Bearer '+token
        }

headers_LKband_alarm = {
            'content-type':
            'application/x-www-form-urlencoded',
            'Authorization':'Bearer '+token_LKband_alarm
        }

slack_webhook_url = "https://hooks.slack.com/services/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"

def send_to_slack(message):
    alert_message = {
        "text": message
    }
    try:
        response = requests.post(slack_webhook_url, json=alert_message)  
        if response.status_code == 200:
            return("Alert sent to Slack!")
        else:
            return("Failed to send alert:" + {response.text})
    except Exception as e:
        print("ERROR, send_to_slack :" + str(e))    

flagcheck = True
flagsend = True
flagdataisnull =  True
count = 0
secound = 60*2
t15 = 0
t70 = 0

t15_alram_point = 23
t70_alram_point = 50

t15_normal_point = 18
t70_normal_point = 45

when_error_will_send_every = 2

delta = datetime.timedelta(hours=1)
now = datetime.datetime.now()
next_hour = (now + delta).replace(microsecond=0, second=0, minute=2)
wait_seconds = (next_hour - now).seconds

msg_error = ""


# Mongo Server Parameters
client = pymongo.MongoClient('mongodb://root:rootpassword@localhost:27017/')
db = client.LK_monitor # use database myDB
coll = db.Monitor

temperatureStats = {
        "sensorName": "Lband",
        "T15": None,
        "T70": None,
        "date": datetime.datetime.now()
    }

# -------------- program start -----------------------------
print("start program : " + str(now))
# sleep(wait_seconds)
now = datetime.datetime.now()
print("start loop" + str(now))

msg = "production service Lband started : " + str(now)
# msg = "service Lband testing: " + str(now)
r = requests.post(url, headers=headers , data = {'message':msg})
r = requests.post(url, headers=headers_LKband_alarm , data = {'message':msg})
send_to_slack(msg)


try: 
    print("try to connect L band")
    app = KatcpClient("192.168.90.113", 53121) # connect to raspi-L 
    while not app.is_connected():
        print("connecting...to raspi_L")
        sleep(0.5)
    print("connected")
except Exception as error:
    print("can't connect to raspi-L " + error)

def send_every_10min():
    global flagsend
    global t15 
    global t70
    state = 0
    while True:
        if(state == 0):
            sleep(60*10)       
            state = 1
        if(state == 1):
            # do case 1 in loop when flagdataisnull is True
            # if send requests.post done do next state again
            sleep(1)
            if(flagdataisnull == False):            
                try:
                    msg = "[SYSTEM LOG Lband]: t15 " + str(t15)+" t70:"+ str(t70)
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    send_to_slack(msg)
                    print(msg)
                    print(r.text)
                    print("DONE, send noti every 10 min")
                    state = 0      
                except Exception as e:
                    print("ERROR, send every 10 min :" + str(e))    

def send_every_12hour():
    global flagsend
    global t15 
    global t70
    state = 0
    while True:
        if(state == 0):
            # sleep(60*1)
            sleep(60*60*1)
            # sleep(60*60*12)    
            state = 1
        if(state == 1):
            # do case 1 in loop when flagdataisnull is True
            # if send requests.post done do next state again
            if(flagdataisnull == False):            
                try:
                    msg = "[SYSTEM LOG Lband]: t15 " + str(t15)+" t70:"+ str(t70)
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    send_to_slack(msg)
                    print(msg)
                    print(r.text)
                    print("DONE, send noti every 12 hr")
                    flagsend = False
                    state = 0      
                except Exception as e:
                    print("ERROR, send every 12 hr :" + str(e))     

def read_and_check():
    global count
    global lband_data_json
    global t15
    global t70
    global flagdataisnull
    global flagcheck
    global temperatureStats
    global msg_error

    '''
    state info:
    -1 = error state
    0 = do every 1 min to read data from receiver
    1 = check condition
    2 = write databese to mongo
    '''
    state = 0
    while True:
        if(state == -1):
            # error state
            msg = "[SYSTEM ERROR Lband]: " + msg_error
            r = requests.post(url, headers=headers , data = {'message':msg})
            send_to_Lband_alarm = requests.post(url, headers=headers_LKband_alarm , data = {'message':msg})
            send_to_slack(msg)
            
            msg_error = ""

            lband_data_json = ""
            t15 = ""
            t70 = ""
            flagdataisnull = True
            state = 0
        if(state == 0):
            # do every 1 minuite
            sleep(secound - time() % secound)
            # clear data 
            lband_data_json = ""
            # read visailar
            try:
                lband_data_json = app.get_lband_data()
                # print(str(ser_bytes))
                # print(str(ser_bytes[0]))
                # sub string
                
                if(lband_data_json != ""):
                    t15 = lband_data_json['dewar']['t15']
                    t70 = lband_data_json['dewar']['t70']
                    t15 = float(t15)
                    t70 = float(t70)
                    print("t15 : " + str(t15) + " t70 : " + str(t70))
                    flagdataisnull = False
                    state = 1
                else:
                    flagdataisnull = True
                    print("lband_data_json = null")
                    state = 0
            except Exception as e:
                print("lband_data_json error:" + str(e))
                msg_error = "state: " + str(state) + " msg: " + str(e)
                state = -1

        if(state == 1): 
            # check condition if system normal
            try:      
                if(flagcheck == False and (t15 < t15_normal_point and t70 < t70_normal_point)):
                    msg = "[SYSTEM INFO Lband]: The system's back to normal."
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    send_to_Lband_alarm = requests.post(url, headers=headers_LKband_alarm , data = {'message':msg})
                    send_to_slack(msg)
                    print(msg)
                    print(r.text)
                    flagcheck = True
            except Exception as e:
                print("check normal error:" + str(e))
                msg_error = "state: " + str(state) + " msg: " + str(e)
                state = -1

            # if system error let send alram every "when_error_will_send_every = x" minute by count parameter
            try:
                if(flagcheck == False):
                    count += 1    
                    if(count >= when_error_will_send_every):
                        msg = "[SYSTEM ERROR Lband] t15: "+ str(t15)+" t70:"+ str(t70)
                        r = requests.post(url, headers=headers , data = {'message':msg})
                        send_to_slack(msg)
                        print(msg)
                        print(r.text)
                        count = 0        
            except Exception as e:
                print("if system error notify every minute error:" + str(e))  
                msg_error = "state: " + str(state) + " msg: " + str(e) 
                state = -1

            # check condition of t15 and t70 are error?
            try:
                if(flagcheck == True and (t15 >= t15_alram_point or t70 >= t70_alram_point)):                
                    msg = "[SYSTEM ERROR Lband]: Please check receiver " + "t15 : " + str(t15)+" t70 : "+ str(t70)
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    send_to_Lband_alarm = requests.post(url, headers=headers_LKband_alarm , data = {'message':msg})
                    send_to_slack(msg)
                    print(msg)
                    print(r.text)
                    flagcheck = False            
            except Exception as e:
                print("check abnormal error:" + str(e)) 
                msg_error = "state: " + str(state) + " msg: " + str(e)
                state = -1

            state = 2
            
        if(state == 2):
            temperatureStats = {
                "sensorName": "Lband",
                "T15": int(t15),
                "T70": int(t70),
                "date": datetime.datetime.now()
                }

            try:
                coll.insert_one(temperatureStats)
                # print("write to mongo done")                    
            except Exception as error:
                print("write_sensor_to_mongo " + error)
                msg_error = "can't write mongo" + "msg: " + str(error)
                state = -1

            state = 0

if __name__ == '__main__':
    read_and_checkThread = threading.Thread(target=read_and_check)
    read_and_checkThread.daemon = True
    read_and_checkThread.start()

    SendEvery12HourThread = threading.Thread(target=send_every_12hour)
    SendEvery12HourThread.daemon = True
    SendEvery12HourThread.start()

    while True:
        try:
            sleep(2)
        except KeyboardInterrupt:
            msg = "stop program Lband: " + str(now) 
            r = requests.post(url, headers=headers , data = {'message':msg})
            send_to_Lband_alarm = requests.post(url, headers=headers_LKband_alarm , data = {'message':msg})
            send_to_slack(msg)
            print("kill")
            # timerThread.join(0.1)       
            read_and_checkThread.join(0.1)
            SendEvery12HourThread.join(0.1)
            # SendEvery10MinThread.join(0.1)
            app.close()
            sys.exit()


