import csv
import requests
import sys
import pandas # sudo apt install python3-pandas, sudo pip3 install pandas
import os
import json
import pymongo
from pymongo import MongoClient
from datetime import datetime as dt, timedelta
import threading
from time import time, sleep
import schedule

# url = 'http://192.168.90.1:9090/api/v1/query?query=node_hwmon_temp_celsius[1m]'
# node_thermal_zone_temp{instance=~"192\\.168\\.90\\.3:9100|192\\.168\\.90\\.4:9100|192\\.168\\.90\\.5:9100|192\\.168\\.90\\.6:9100|192\\.168\\.90\\.7:9100|192\\.168\\.90\\.8:9100|localhost:9100"}
# node_hwmon_temp_celsius{instance=~"192\\.168\\.90\\.1:9100|192\\.168\\.90\\.2:9100|192\\.168\\.90\\.3:9100|192\\.168\\.90\\.4:9100|192\\.168\\.90\\.5:9100|192\\.168\\.90\\.6:9100"}
# url = 'http://192.168.90.1:9090/api/v1/query?query=node_thermal_zone_temp{instance=~"192.168.90.1:9100|192.168.90.2:9100"}[2m]'


# line-notification
url = 'https://notify-api.line.me/api/notify'
token_Backend_alarm = 'OqMzFbzExLRSbIYoLsd1zvS4SrFwsUtsmZV18ZdBUDW'
headers_Backend_alarm = {
        'content-type':
        'application/x-www-form-urlencoded',
        'Authorization':'Bearer '+token_Backend_alarm
        }

# globla variable
ip_dress_promethus = "http://192.168.90.1:9090/api/v1/query?query="
secound = 60
list_node_thermal_zone_temp = []
flag_run = False
msg_error = ""

def datetime_from_utc_to_local(utc_float):
    date = dt.fromtimestamp(utc_float).strftime("%Y/%m/%d %H:%M:%S")
    date_time_obj = dt.strptime(date, '%Y/%m/%d %H:%M:%S')
    return date_time_obj

def read_phometheus(query_name, instance, duration):
    if(query_name == "node_thermal_zone_temp"):
        during = '{' + "instance=~" +  '\"' + instance +'\"' + '}'+ '[' + duration + ']'  
        url = ip_dress_promethus+query_name+during

        response = requests.get(url)
        status = response.json()['status']
        data = response.json()['data']
        result = response.json()['data']['result']
        # print(result)
        if(status == "success"):
            return response
        else:
            return False

def handle_data_node_thermal_zone_temp(ack):
        data = ack.json()['data']
        result = ack.json()['data']['result']
        # df = pandas.DataFrame(result)
        # print(df)

        temp_list = [] # empty list
        for metric in result:
            # print(metric)
            name = metric['metric']['__name__']
            inst = metric['metric']['instance']
            job = metric['metric']['job']
            s_type = metric['metric']['type']
            zone = metric['metric']['zone']
            values = metric['values']

            for value in values:
                date_local = datetime_from_utc_to_local(value[0])
                temp = value[1]
                # print(date_local)
                # inst, name, zone, date, temperature
                # 192.168.90.1, node_thernal_zone_temp, 0, 21012023, 32
                temperatureStats = {
                    "date": date_local,
                    "instance": inst,
                    "sensorName": name,
                    "zone": zone,
                    "temp": int(temp)
                }

                temp_list.append(temperatureStats)
        return temp_list

def read_and_check():
    global list_node_thermal_zone_temp
    global flag_run
    global msg_error
    '''
    state info:
    -1 = error state
    0 = do every 24 hour to read data from prometheus
    1 = write databese to mongo
    2 = export to csv
    '''
    flag_run = True
    state = 0
    while flag_run:
        if(state == -1):
            # error state
            msg = "[supvervisor/server/2_prometheuse ERROR]: " + msg_error
            r = requests.post(url, headers=headers , data = {'message':msg})
            # reset variable
            list_node_thermal_zone_temp = []
            state = 0

        if(state == 0):
            # do every 24 hours
            sleep(10)
            # clear data 
            list_node_thermal_zone_temp = []
            # read sensor
            try:
                # instace = "192.168.90.1:9100|192.168.90.2:9100|192.168.90.3:9100|192.168.90.4:9100|192.168.90.5:9100|192.168.90.6:9100|192.168.90.7:9100|192.168.90.8:9100"
                instace = "192.168.90.1:9100" # for test
                duration = "24h"
                ack = read_phometheus("node_thermal_zone_temp", instace, duration)
                if(ack != False):
                    list_node_thermal_zone_temp = handle_data_node_thermal_zone_temp(ack)
                    state = 1
                else:
                    state = 0
            except Exception as e:
                msg_error = "state 0: " + str(e) 
                print("state 0 read sensor error:" + str(e))
                state = -1

        if(state == 1): 
            # write databese to mongo
            try:      
                mongo_client = pymongo.MongoClient('mongodb://root:rootpassword@192.168.90.1:27017/')
                db = mongo_client.prometheus # use database myDB
                coll = db.Monitor
                                
                if(len(list_node_thermal_zone_temp) > 0):
                    for row in list_node_thermal_zone_temp:
                        coll.insert_one(row)

                    print("inserted")

                mongo_client.close()
                state = 2
            except Exception as e:
                mongo_client.close()
                msg_error = "state 1: " + str(e)
                print("state 1 read sensor error:" + str(e))
                state = -1

        if(state == 2): 
            # export to csv
            try:
                today = dt.today()
                yesterday = today - timedelta(days = 1)
                yesterday = yesterday.strftime('%Y-%m-%d')
                data_export = pandas.DataFrame(list_node_thermal_zone_temp)
                # print(data_export)

                output_dir = "/mnt/tnrt/dev/log/server/node_thermal_zone_temp/"
                # output_dir = "./"
                output_filename = "node_thermal_zone_temp" + "_" + str(today) +".csv"  
                output_path = output_dir + output_filename
                print("output_path: " + output_path)
                isExist = os.path.exists(output_path)

                if(isExist == True):
                    print("file name already exist on this path")
                else:
                    # export MongoDB documents to a CSV file, leaving out the row "labels" (row numbers)
                    data_export.to_csv(output_path, ",", index=False) # CSV delimited by commas
                    print("Export done")
                    flag_run = False # stop this while loop

            except Exception as e:
                msg_error = "state 2: " + str(e)
                print("state 2 read sensor error:" + str(e))
                state = -1

        state = 0

if __name__ == '__main__':
    schedule.every().day.at("00:00").do(read_and_check)
    # schedule.every(1).minutes.do(read_and_check)

    while True:
        try:
            schedule.run_pending()
            sleep(65)
        except KeyboardInterrupt:
            print("kill")
            read_and_checkThread.join(0.1)
            sys.exit()
