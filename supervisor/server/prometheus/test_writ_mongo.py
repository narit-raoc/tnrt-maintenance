import pymongo
from pymongo import MongoClient
import datetime

# Server Parameters
client = pymongo.MongoClient('mongodb://root:rootpassword@192.168.90.1:27017/')

db = client.prometheus # use database myDB
coll = db.Monitor

temperatureStats = {
        "date":  datetime.datetime.now(),
        "instance": "192.168.90.1",
        "sensorName": "node_thermal_zone_temp",
        "zone": 1,
        "temp": int(35)
    }



if(temperatureStats['instance'] == "192.168.90.1"):
  coll.insert_one(temperatureStats)


for x in coll.find():
  print(x)
