import pymongo
from pymongo import MongoClient
import datetime

# Server Parameters
client = pymongo.MongoClient('mongodb://root:rootpassword@192.168.90.1:27017/')

db = client.prometheus # use database myDB
coll = db.Monitor

# data_row = coll.find_one()

# for data_row in coll.find():
#   print(data_row)


last_N = 10
# query last_N data from collection
data_row = coll.find().skip(coll.count() - last_N)
for x in data_row:
  print x
