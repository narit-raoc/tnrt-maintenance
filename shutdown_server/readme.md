# script shutdown all servers
Run this script to shutdown all server at once from server 90.1 when electricity black out.

## setup environment variables
copy .env.example and create new file name .env and fill out all credentials needed

In repository directory,
```
cp .env.example .env
```
